#!/bin/bash

# This is a script that runs my A* algorithm on numerous Sliding tile puzzle states.
A_Star_tests_states() {
    filename='resultsFile.txt'
    n=0
    dict=()
    while read line; do
    # reading each line
    dict[$n]="korf100/${line:4:6}"
    n=$((n+1))
    done < $filename
    echo ${dict[@]}
}

tests_states="$(A_Star_tests_states)"

test_run(){
    test_type=$1
    searchFile='Search_algorithm.py'
    for kfile in $tests_states; do
    inputFileName="$kfile"
    echo $inputFileName
    time python3 $searchFile -t $1 -f $inputFileName
    echo
    echo "Done"
    echo 
    done

}
test_run "a_star"