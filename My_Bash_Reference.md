# Basics of Bash
## Action-command
### print working directory - *pwd*
### Navigating through directories- *ls* and *cd*
* lists the contents of the current directory - *ls*
* *ls <folder>* : to see the contents of a particular folder.
* *ls -a* : For listing all the hidden files in a folder
* *ls -l* : Prints out a longer and more detailed listing of the files. ls -l can also be used with the name of the Directory to list the files of that particular directory.
* *ls ~* : tilde(~) is a short cut which denotes the home directory. So, regardless of what directory we are into, ls ~ will always list the home directory.
### Change directories - *cd <directory>*
* *d ..* : To go back to the parent directory.
* *cd* : To go back to the home directory
### Organizing Directories
* make directory - *mkdir*
* move one or more files or directories from one place to another - *mv <source> <destination>*
* create new, empty files - *touch <filename>*
* remove files or directories - *rm <filename>* or *rmdir <directory_name >*
* it reads a file and outputs its content - *cat <filename>* NB: To view more than one file, mention both the filenames after the *cat*
* lets us view the contents of files, one screen at a time *less <filename> <filename>*
    Spacebar : To go to the next screen
    b: to go to the previous screen
    /: to search for a specific word
    q: quit
* gives information about the <keyword> command - *man <keyword>* 
* copies the content of one file/directory into the other file/directory - *cp <file/directory_one> <file/directory_two>*

### Pipelines and Filters
* send the output of one command as an input to another command - *pipe the output of "this" into "that" - <this> | <that>*
* searches for lines with a given string or looks for a pattern in a given input stream - *grep <search_string> <search_file>* adding *-v* gets the lines that do not have the search string
* word count - *wc*
* gives us the number of unique lines in the input stream - *uniq <search_file>*
* returns the difference between the two files - *diff <file_one> <file_two>*
* redirect the output of a file to a file - *>* 

# How to bash
You can see your "dot files" by entering - *ls -a ~/.bashrc*. The -a tells it to print all files including the dot files.

## Bash Aliases
A Bash alias is a method of supplementing or overriding Bash commands with new ones. Bash aliases make it easy for users to customize their experience in a POSIX terminal. They are often defined in $HOME/. bashrc. See the aliases available in your shell environment with the alias command
## Bash Functons
They have an easy syntax.
*function_name () {*
  *# logic here*
*}*
You can run Bash scripts directly from your terminal program.
Example:

    wbboinde19@LAPTOP-79J5AMEV:/mnt/c/Users/boind/wisdom-boinde$ test () {
    > if [ -z "$1" ]; then # no argument
    >   echo "Hi, anonymous!";
    > else
    >   echo "Hi, $1 $2. We are using Bash functions!";
    > fi
    > }
    wbboinde19@LAPTOP-79J5AMEV:/mnt/c/Users/boind/wisdom-boinde$ test
    Hi, anonymous!
    wbboinde19@LAPTOP-79J5AMEV:/mnt/c/Users/boind/wisdom-boinde$ test Wisdom Boinde
    Hi, Wisdom Boinde. We are using Bash functions!
    wbboinde19@LAPTOP-79J5AMEV:/mnt/c/Users/boind/wisdom-boinde$ 
# Understand what stdin, stdout, and stderr are
They are three data streams created when you llunch a linux command. You can use them to tell if your script is being piped or redirected.
## stdin
The linux standard input stream. It accepts texts as an input.
## stdout
the text output from a command to the shell is delivered through stdout
## stderr
Error messages from the command are sent through the stderr stream.
# command line recall- ^a, ^e, ^r, history
* *^a* moves our cursor to the beginning of the line
* *^e* moves our cursor to the end of the line
* *^r* Searching backwards in history. you can type ^r and begin to type out part of the previous command. If it matches an uwanted, type ^r again to see the next results.
# Pipes and redirection-Capturing both stdout and stderr to one file
When you create a script which displays a stdout and stderr, you cap capture them to a file. when redirecting, the stdout stream sends only the stdout and not the stderr. Thus to send both use - *./error.sh > capture.txt 2>&1*
These values are always used for stdin, stdout, and stderr:
* 0: stdin
* 1: stdout
* 2: stderr
# The for Loop
The syntax for loop is *for NAME [in LIST ]; do COMMANDS; done*
when you loop through a file, to specify an entry, use $index
    for i in cpofwisdom-boinde.txt ; do cat "$i" ; done  
this takes each element in cpofwisdom-boinde and *cat* each element
while loops syntax is 
    *while CONTROL-COMMAND; do CONSEQUENT-COMMANDS; done*

*while [ $i -lt 4 ]*
*do*
*command*
*i=$[$i+1]*
*done*

echo {0..10} - Prints out numbers from 1 to 10
echo {10..0..2} - prints every second number, starting with 10 and making its way backwards to 0.

Suppose you have a variable like: a="Too longgg"
    echo ${a%gg}
chops off the last two gs and prints “Too long“
NB - *% tells the shell you want to chop something off the end of the expanded variable*

Substitution
    ${FOO%suffix}	Remove suffix
    ${FOO#prefix}	Remove prefix
    ${FOO%%suffix}	Remove long suffix
    ${FOO##prefix}	Remove long prefix
    ${FOO/from/to}	Replace first match
    ${FOO//from/to}	Replace all
    ${FOO/%from/to}	Replace suffix
    ${FOO/#from/to}	Replace prefix

# Variables
## Global Variables
Global variables or environment variables are available in all shells.The *env* or *printenv* commands can be used to display environment variables. Shell variables are in uppercase characters by convention
## Local Variables
local variables are only available in the current shell. Using the set built-in command without any options will display a list of all variables (including environment variables) and functions.

Variables can also be devided by content: 
* String variables
* Integer variables
* Constant variables 
* Array Variables 
## Creating Variables
Variables are case sensitive and capitalized by default. Giving local variables a lowercase name is a convention which is sometimes applied. However, you are free to use the names you want or to mix cases. Variables can also contain digits, but a name starting with a digit is not allowed.
To set a variable in the shell, use
*VARNAME="value"* Do not leave space around the "="
example: 
    wbboinde19@LAPTOP-79J5AMEV:/mnt/c/Users/boind$ myName="Wisdom"
    wbboinde19@LAPTOP-79J5AMEV:/mnt/c/Users/boind$ echo $myName
    Wisdom

## Exporting Variables
Creating a variable using *VARNAME="value"* creates a local variable. It is only available in the current shell.child processes of the current shell will not be aware of this variable. In order to pass variables to a subshell, we need to export them using the export built-in command. Variables that are exported are referred to as environment variables. This is done by:

*export VARNAME="value"*

## Special Parameters
The shell treats several parameters specially. These parameters may only be referenced; assignment to them is not allowed.

Character	Definition
* $*	Expands to the positional parameters, starting from one. When the expansion occurs within double quotes, it expands to a single word with the value of each parameter separated by the first character of the IFS special variable.
* $@	Expands to the positional parameters, starting from one. When the expansion occurs within double quotes, each parameter expands to a separate word.
* $#	Expands to the number of positional parameters in decimal.
* $?	Expands to the exit status of the most recently executed foreground pipeline.
* $-	A hyphen expands to the current option flags as specified upon invocation, by the set built-in command, or those set by the shell itself (such as the -i).
* $$	Expands to the process ID of the shell.
* $!	Expands to the process ID of the most recently executed background (asynchronous) command.
* $0	Expands to the name of the shell or shell script.
* $_	The underscore variable is set at shell startup and contains the absolute file name of the shell or script being executed as passed in the argument list. Subsequently, it expands to the last argument to the previous command, after expansion. It is also set to the full pathname of each command executed and placed in the environment exported to that command. When checking mail, this parameter holds the name of the mail file.

positional parameters are the words following the name of a shell script. They are put into the variables $1, $2, $3 and so on. As long as needed, variables are added to an internal array. $# holds the total number of parameters.

A lot of keys have special meanings in some context or other. Quoting is used to remove the special meaning of characters or words: quotes can disable special treatment for special characters, they can prevent reserved words from being recognized as such and they can disable parameter expansion.

Escape characters are used to remove the special meaning from a single character. A non-quoted backslash, \, is used as an escape character in Bash:
    ~>date=20021226
    ~>echo $date
    20021226
    ~> echo \$date
    $date

Single quotes ('') are used to preserve the literal value of each character enclosed within the quotes:
    echo '$date'
    $date

## Shell Expansion

Brace expansion is a mechanism by which arbitrary strings may be generated. Patterns to be brace-expanded take the form of an optional PREAMBLE, followed by a series of comma-separated strings between a pair of braces, followed by an optional POSTSCRIPT. The preamble is prefixed to each string contained within the braces, and the postscript is then appended to each resulting string, expanding left to right.
Brace expansions may be nested. The results of each expanded string are not sorted; left to right order is preserved:
example:
    ~> echo sp{el,il,al}l
    spell spill spall

# Creating and running a script
A shell script is a sequence of commands for which you have a repeated use. This sequence is typically executed by entering the name of the script on the command line
To Add the directory to the contents of the PATH variable:
    export PATH="$PATH:~/scripts"
## Executing the script
The script should have execute permissions for the correct owners in order to be runnable. When setting permissions, check that you really obtained the permissions that you want. When this is done, the script can run like any other command:
To make a script executable - *chmod +x <script_name>* chmod-change file access permission
run the script- *./<script_name>*

## Script Basics
When running a script in a subshell, you should define which shell should run the script. The shell type in which you wrote the script might not be the default on your system, so commands you entered might result in errors when executed by the wrong shell.

The first line of the script determines the shell to start. The first two characters of the first line should be #!, then follows the path to the shell that should interpret the commands that follow. Blank lines are also considered to be lines, so don't start your script with an empty line.
    #!/bin/bash
This uses the Bash Executable.

The # is used to add comments

To Debug, the most common is to start up the subshell with the -x option, which will run the entire script in debug mode. Traces of each command plus its arguments are printed to standard output after the commands have been expanded but before they are executed.
example:
    ~/scripts> bash -x script1.sh
To debug on Parts of the script,
    set -x			# activate debugging from here
    w
    set +x			# stop debugging from here

# Commonly used commands: tar, ps, cat, grep (egrep), sed, awk, split

## tar
The Linux ‘tar’ stands for tape archive, is used to create Archive and extract the Archive files. 
Syntax: tar [options] [archive-file] [file or directory to be archived]

An Archive file is a file that is composed of one or more files along with metadata. Archive files are used to collect multiple data files together into a single file for easier portability and storage, or simply to compress files to use less storage space.

### Options:
* -c : Creates Archive
* -x : Extract the archive
* -f : creates archive with given filename
* -t : displays or lists files in archived file
* -u : archives and adds to an existing archive file
* -v : Displays Verbose Information
* -A : Concatenates the archive files
* -z : zip, tells tar command that create tar file using gzip
* -j : filter archive tar file using tbzip
* -W : Verify a archive file
* -r : update or add file or directory in already existed .tar file

-cvf : This command creates a tar file called file.tar which is the Archive of all .c files in current directory.

## ps
In Linux, a running instance of a program is called process. Occasionally, when working on a Linux machine, you may need to find out what processes are currently running. ps lists the currently running processes and display information about those processes.

Syntax: ps [OPTIONS]

NB similar to *top*

## sed
Stands for Stream Editor. It can perform lot’s of function on file like, searching, find and replace, insertion or deletion
    $sed 's/Boy/Girl/' myfile.txt
Replaces the word “Boy” with “Girl” in the file. Here the “s” specifies the substitution operation. The “/” are delimiters. The “Boy” is the search pattern and the “Girl” is the replacement string.
To specify which ocurance to replace, add n of the nth ocurance after the last /. Thus, $sed 's/Boy/Girl/2 for the second ocurance of Boy.
To replace line by line, use */g* at end. To replace after the nth ocurance in a line, */ng*

## Awk
Awk is a scripting language used for manipulating data and generating reports
### AWK Operations: 
* Scans a file line by line 
* Splits each input line into fields 
* Compares input line/fields to pattern 
* Performs action(s) on matched lines 



### Useful For: 
* Transform data files 
* Produce formatted reports 

### Programming Constructs: 
* Format output lines 
* Arithmetic and string operations 
* Conditionals and loops 

    Syntax: awk options 'selection _criteria {action }' input-file > output-file

### Options: 

-f program-file : Reads the AWK program source from the file 
                  program-file, instead of from the 
                  first command line argument.
-F fs            : Use fs for the input field separator

### Sample Commands 

Printing lines which matches a given partten: $ awk '/String of partern/ {print}' file.txt

Splitting a Line Into Fields : $ awk '{print $1,$4}' file.txt 

 ## Split
 Split command in Linux is used to split large files into smaller files. It splits the files into 1000 lines per file(by default) and even allows users to change the number of lines as per requirement.
    Syntax: split [options] name_of_file prefix_for_new_files
After it splits a file, i creates new file, you can given those files a common prefix.

To Split file based on number of lines: 
    split -l 4 index.txt split_file
Where 4 is the number of lines.

# Job control with nohup, &, ^z, bg, and fg

Ctrl-Z: Suspend the process running in the foreground by sending the signal SIGTSTP
fg: Move a background job into the foreground
bg: Resume suspended jobs by running them as background jobs
jobs: Display a list of the jobs with their status
If a command ends with the & operator: the shell will run the command in the background in a subshell
nohup: it can protect a command from the SIGHUP signal
    $ nohup ./long_running.sh

# Regular expressions
A way to search through a string of text.
**Examples:**
* Using the *?*:
> egrep 'Wise?dom' Test_file.txt - Searches for both Wisdom and Wisedom. We want either zero or 1 "e" 

* Using the *:
> ls T* - display items in the current directory which start with the letter "T"
> ls *T - display items which end with the letter "T"
> ls *T* - display items with T inside them
> ls wisdom-boinde/*.md - display items in wisdom-boinde directory which end with .md

* Using the . character:
> grep b.y Test_file.txt - searches any line with any character between b and y 

* Using []:

To find a subset of any character.

> egrep [a-d]oy Test_file.txt - searches for the lines with a character between a and d inclusive infront of "oy"

> If you do not separate the characters in the [], it picks them one by one but not more than one.

> egrep "b[aout]y" Test_file.txt - seached for lines with b[one of the guys in the brackets]y

> egrep "w[a-zA-Z0-9_]boinde19" Test_file.txt - returns wbboinde19, wBboinde19, w9boinde19

> egrep "^boy" Test_file.txt - Looks for lines that start with boy

> egrep "^5" command-line-examples/ints-5.dat | egrep "*66*" | egrep "12$" - performs successive actions in finding a number in command-line-examples/ints-5.dat that start with 5, end with 12, has a 66 with at least one character on either side.   

* Quantifiers:

+ - one or more of a character
? - 0 or one of a character
* - zero or more of a character
{3}- a certain number or range of characters. 3 here is that number/range of characters 

*if you put a slash infrom of a character that means something, it doesnt any longer. It just means itself.*

* Groups:
> ( - Starts a groups
> ) - ends the group 
> [a-z]+ - any number of letters
> _ - a simple match

egrep -r search_string * - search all the files in the current directory and subdirectories for search_string

egrep -i search_string *.dat - case insensitive search

egrep -w search_string file_to_search - search for full word only and not as sub-string

egrep -l search_string *.dat - list only the file names that contain a match, not all the matches themselves

egrep -o search_string *.dat - list only the matched string, not the full line it appears on (super handy for counting occurences when piped into wc -l)