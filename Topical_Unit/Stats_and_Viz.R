Korf_data <- read.csv("Test2.csv")
# To print out the stats of the data
summary(Korf_data)
matrx=data.matrix(log(Korf_data[,2:5]))
# To plot a bar graph of the data,
colors=c('red', 'yellow','green','blue')
Puzzles=Korf_data$Puzzle.number
model=c('Unit cost','Heavy cost','Inverse cost','Reverse cost')

barplot(t(matrx),beside=TRUE,names.arg = Puzzles, col = colors,las=2,main = "Nodes expanded for the different cost models")
legend("topleft", model, cex = 0.5, fill = colors)
#This Visualization is a barplot of the Korf100 easy puzzles. It shows the number of nodes expanded by the different cost models
#On the different Puzzles.