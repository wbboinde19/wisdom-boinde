# Wisdom Boinde
My name is Wisdom Boinde, I am a sophomore and I plan on majoring in Computer science and Physics

## A description of the course
This is a course that teaches the foundational technical skills that can be applied in the CS curriculum and other areas of life that depend on depend on technical computing. The course provides resources and deliverables for students as well as support and feedback.
This is a module-based course with an emphasis on independent work. It consists of a base unit, composed of 7 sub-units, and one topical unit. The successor course, CS267, is composed of two topical units. By completing these units students gain proficiency in using the important skills to complete more advanced projects.
Each unit builds on earlier ones, using tools and techniques learned earlier, and each with one or more deliverables to demonstrate your proficiency. There are two required weekly check-ins, one short meeting (either in-person or via internet telephony) and one Moodle-based quiz to help keep track of students progress.

## My Units
### Fundamental computing skills
This teaches component skills that have a wide application across computer science. The subunits addressed here are:
* *Command line utilities and Bash*
* *Version control with Git*
* *Markdown for composing readable text*
* *Introduction to regular expressions*
* *C programming language*
* *Building and installing software*
* *Batch computing and scheduling*
In learning these subunits, we acquired skills in Bash scripting, using the terminal, source control, writing a markdown, regular expresions, and so many more. 
**A working link to the Moodle page:** https://moodle.earlham.edu/course/view.php?id=4335

A section displaying the code for "Hello World" in C

    #include <stdio.h>
    int main()
    {
        printf("Hello World");
        return 0;
    }

