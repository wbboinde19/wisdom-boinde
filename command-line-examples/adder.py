import sys
import os.path
import argparse

def addIntegers(inputFileName):
	theSum = 0

	infile = open(inputFileName, "r")
	infileLines = infile.readlines()

	for x in infileLines:
		intX = int(x.strip())
		theSum += intX
		# theSum = theSum + intX

	return(theSum)

if __name__ == "__main__":
	parser = argparse.ArgumentParser(description='automated testing demo')
	parser.add_argument('-i','--inputFileName', type=str, help='File of integers, one per line', required=True)
	args = parser.parse_args()

	if not (os.path.isfile(args.inputFileName)):
		print("error,", args.inputFileName, "does not exist, exiting.", file=sys.stderr)
		exit(-1)

	print(addIntegers(args.inputFileName))