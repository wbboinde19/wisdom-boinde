    // Read a file containing an arbitrary number of integers, one per line. Display the count, total, and average on stderr 
// at the end of the run. Make the program capable of displaying the maximum values of each quantity without resorting to 
// arbitrary precision arithmetic (hint, unsigned long int).
    // Use a function as part of your program, e.g. to write the aggregated values to stdout.
    // Use command line parameters available via argv and argc to support -i input-file-name to pass the input file to your program.

#include <stdio.h>
#include <stdlib.h>
int average(long int t,int c);

int main(int argc, char *argv[]){
    int count = 0;
    long int total = 0;
    long int max = 0;
    long int num, i;
    long int ave;
    FILE *ints;
    ints = fopen(argv[1],"r");

    if(ints == NULL)
   {
       printf("Error!");
       exit(1);
   }
    // Loop from beginning to end
    for (i = 0; i < 19; ++i) {
        // get number and store in num
        fscanf(ints, "%ld", &num);
        // update count, total, and max
        count+=1;
        total+=num;
        if (num>max){
            max = num;
        }

    }  
    ave = average(total,count);
    // Work on display
    printf("count: %d, total: %ld, and average: %ld\n", count, total, ave);
    

    fclose(ints);
}

int average(long int t,int c){
    long int av;
    av=t/c;
    return av;
}